Connor Campi

connor@campi.cc

Prog3_1 is a c++ program that takes a lua program as a command line argument and then executes it.

Prog3_2 is a lua file that includes a function InfixToPostfix, which takes one argument which is a series of mathematical operations in "infix" form. The function takes these operations and outputs the same series in "postfix" form.

Prog3_2 is a c++ program that takes a line of input from stdin and pushes it through Prog3_2's InfixToPostfix function.