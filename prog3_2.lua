object = {}

function object:new(t)
  local obj = t or {}
  setmetatable(obj,self)
  self.__index = self
  return obj
end

stack = object:new{
	top = 0,
	data = {}
}

function stack:push(x)
	self.top = self.top + 1
	self.data[self.top] = x
end

function stack:pop()
	if(self.top == 0) then
		return "no"
	else
		local ret = self.data[self.top]
		self.data[self.top] = nil
		self.top = self.top - 1
		return ret
	end
end

function stack:peek()
  if(self.top == 0) then
    return "no"
  else
	  return self.data[self.top]
	end
end

function stack:notEmpty()
  if(self.top ~= 0) then
    return true
  else
    return false
  end
end

queue = object:new{
	first = 0,
	last = -1,
	data = {}
}

function queue:enqueue(x)
	self.last = self.last + 1
	self.data[self.last] = x
end

function queue:dequeue()
	if (self.first > self.last) then
		return "no"
	else
		local ret = self.data[self.first]
		self.data[self.first] = nil
		self.first = self.first + 1
		return ret
	end
end

function queue:peek()
  if(self.first > self.last) then
    return "no"
  else
	  return self.data[self.first]
	end
end

function queue:notEmpty()
  if(self.first > self.last) then
    return false
  else
    return true
  end
end

tokenizer = object:new{
	qu = queue:new()
}

function tokenizer:tokenize(x)
	for token in string.gmatch(x, "%S+")
	do
		self.qu:enqueue(token)
	end
end

function tokenizer:getToken()
	return self.qu:dequeue()
end

function tokenizer:hasTokens()
  return self.qu:notEmpty()
end

function isOperator(x)
  if(x=="*" or x=="/" or x=="-" or x=="+") then
    return true
  else
    return false
  end
end

function InfixToPostfix(i)
  
	ops = stack:new()
	out = queue:new()
	tok = tokenizer:new()
	tok:tokenize(i)
	out:enqueue(tok:getToken())
	ops:push(tok:getToken())
	
	while(tok:hasTokens())
	do
	  next = tok:getToken()
    if(tonumber(next) ~= nil) then
        out:enqueue(next)
    else
      if(next == "(") then
        ops:push(next)
      else
        if(next ==")") then
          while(ops:notEmpty() and ops:peek() ~= "(")
          do
            out:enqueue(ops:pop())
          end
          ops:pop()
        end
      end
      
      endwhile = 0
      if(isOperator(next)) then
        if(ops:notEmpty()) then
          while(ops:notEmpty() and endwhile == 0)
          do
            if(next == "+" or next == "-") then
                out:enqueue(ops:pop())
            end
            if((next =="*" or next=="/") and (ops:peek() ~= "+" and ops:peek() ~= "-")) then
              out:enqueue(ops:pop())
            end
            if((next =="*" or next=="/") and (ops:peek() == "+" or ops:peek() == "-")) then
                  endwhile = 1
              end
          end
        end
        ops:push(next)
      end
    end
  end
  
  opOut = stack:new()
  while(ops:notEmpty())
  do
    opOut:push(ops:pop())
  end
  
  
  while(opOut:notEmpty())
  do
    out:enqueue(opOut:pop())
  end
  write = " "
  
  if(out:notEmpty()) then
    write = tostring(out:dequeue())
    while(out:notEmpty())
    do
      write = write.." "
      write = write .. tostring(out:dequeue())
    end
  end
  
  print(write)
end