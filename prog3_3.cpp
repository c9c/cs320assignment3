#include <lua.hpp>
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc,char *argv[]) {
	cout << "Assignment #3-3, Connor Campi, connor@campi.cc" << endl;

    lua_State *L = luaL_newstate();
	luaL_openlibs(L);

    luaL_loadfile(L,argv[1]);

	char buff[256];
	scanf("%255[^\n]",buff);

	luaL_dostring(L, string str("InfixToPostfix(",15) + buff + string str(")",1))

	printf(luaL_checkstring(L,"write"));

    lua_close(L);
    return(0);
}
